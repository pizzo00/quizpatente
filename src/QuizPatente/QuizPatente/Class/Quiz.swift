//
//  Quiz.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 24/01/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class Quiz: NSObject
{
    public var Patente : Patente?
    public var Capitoli : [Int]?
    public var Domande : [Domanda]
    public var Success : () -> Void
    public var Error : () -> Void
    public var Complete : () -> Void
    public var Result : Static.ResultType = .NotComplete
    
    var url : URL?
    
    
    init(_patente : Patente?, _nDomande : Int, _success : @escaping () -> Void, _error : @escaping () -> Void, _complete : @escaping () -> Void, _capitoli : [Int]? = nil)
    {
        self.Patente = _patente
        self.Capitoli = _capitoli
        self.Domande = []
        self.Success = _success
        self.Error = _error
        self.Complete = _complete
        
        var formattedCapitoli = ""
        if(self.Capitoli != nil)
        {
            for capitolo in self.Capitoli!
            {
                formattedCapitoli += String(capitolo) + ","
            }
        }
        
        let stringUrl = String(format: "https://www.quizpatenteapp.com/api/quiz/?f=get_questions&database=%@&chapters=%@&num_of_question=%d&type=4", self.Patente!.Codice, formattedCapitoli, _nDomande)
        self.url = URL(string: stringUrl)
        super.init()
        
        self.getDomande(nDomande: _nDomande)
    }
    
    func getDomande(nDomande : Int)
    {
        if nDomande <= 0
        {
            self.Domande = []
            self.Result = .Error
            self.Error()
            self.Complete()
            return
        }
        
        URLSession.shared.dataTask(with: self.url!)
        {
            data,
            response,
            error in guard error == nil,
            
            let data = data
            else
            {
                print(error!) //TODO Gestire errori
                self.Domande = []
                self.Error()
                self.Complete()
                return
            }
            
            
            self.Domande = []
            if let dict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [[String : Any]]
            {
                if dict!.count == 0
                {
                    self.Domande = []
                    self.Error()
                    self.Complete()
                    return
                }
                
                for domanda in dict!
                {
                    self.Domande.append(Domanda(dict: domanda))
                }
            }
            else
            {
                self.Domande = []
                self.Error()
                self.Complete()
                return
            }
            
            self.Result = .Success
            self.Success()
            self.Complete()
        }.resume()
    }
}
