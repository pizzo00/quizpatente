//
//  Patente.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 31/01/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class Patente: NSObject
{
    public var Codice : String
    public var Nome : String
    public var NDomandeDefault : Int
    public var MinimoRisposteCorrette: Int
    public var TempoMassimo : Int
    public var SupportaEsamePersonalizzato: Bool
    
    init(_codice : String,
        _nome : String,
        _nDomandeDefault : Int,
        _minimoRisposteCorrette: Int,
        _tempoMassimo : Int,
        _supportaEsamePersonalizzato: Bool = true)
    {
        self.Codice = _codice
        self.Nome = _nome
        self.NDomandeDefault = _nDomandeDefault
        self.MinimoRisposteCorrette = _minimoRisposteCorrette
        self.TempoMassimo = _tempoMassimo
        self.SupportaEsamePersonalizzato = _supportaEsamePersonalizzato
    }

}
