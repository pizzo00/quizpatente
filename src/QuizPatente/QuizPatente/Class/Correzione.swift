//
//  Correzione.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 24/01/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class Correzione: NSObject
{
    public var Domanda : Domanda
    public var Titolo : String = ""
    public var Testo : String = ""
    public var Success : () -> Void
    public var Error : () -> Void
    public var Complete : () -> Void
    
    var url : URL?
    
    init(_domanda : Domanda, _success : @escaping () -> Void, _error : @escaping () -> Void, _complete : @escaping () -> Void)
    {
        self.Domanda = _domanda
        self.Success = _success
        self.Error = _error
        self.Complete = _complete
        
        let stringUrl = String(format: "https://www.quizpatenteapp.com/api/theory/?f=get_suggestion&qid=%d", self.Domanda.Id)
        self.url = URL(string: stringUrl)
        super.init()
        
        URLSession.shared.dataTask(with: self.url!)
        {
            data,
            response,
            error in guard error == nil,
                
            let data = data else
            {
                print(error!) //TODO Gestire errori
                
                self.Error()
                self.Complete()
                return
            }
            
            
            if let dict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any]
            {
                self.Titolo = dict!["title"] as! String
                self.Testo = dict!["description"] as! String

                self.Testo = self.Testo.replacingOccurrences(of: "<br />", with: "\n")//Replace html tag
                self.Testo = self.Testo.replacingOccurrences(of: "(\\r|\\n)+", with: "\n", options: .regularExpression)//Remove duplicate new line, carriage return
                
                
            }
            
            self.Success()
            self.Complete()
        }.resume()
    }

}
