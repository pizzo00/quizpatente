//
//  Domanda.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 24/01/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class Domanda: NSObject
{
    public var Id : Int
    public var Testo : String
    public var Figura : Immagine?
    public var RispostaCorretta : Bool
    public var RispostaUtente: Bool? = nil
    public var IdTeoria : Int
    public var Teoria : Correzione? = nil
    public var IdCapitolo : Int
    
    
    init(dict : [String : Any])
    {
        self.Id = dict["id"] as! Int
        self.Testo = dict["question"] as! String
        self.RispostaCorretta = dict["answer"] as! Bool
        self.IdTeoria = dict["theory"] as! Int
        self.IdCapitolo = dict["id_chapter"] as! Int
        
        let idImmagine = dict["image"] as! Int
        if idImmagine != 0
        {
            self.Figura = Immagine(_id: idImmagine)
        }
    }
    
    func getCorrezione(success : @escaping () -> Void, error : @escaping () -> Void, complete : @escaping () -> Void)
    {
        if self.IdTeoria != 0
        {
            self.Teoria = Correzione(_domanda: self, _success: success, _error: error, _complete: complete)
        }
        else
        {
            success()
            complete()
        }
    }
}
