//
//  Static.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 07/02/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class Static: NSObject
{
    static var QuizCorrente : Quiz? = nil
    
    static private let AM =          Patente(_codice: "AM",       _nome: "AM",         _nDomandeDefault: 30, _minimoRisposteCorrette: 36, _tempoMassimo: 30)
    static private let AB =          Patente(_codice: "AB",       _nome: "A-B",        _nDomandeDefault: 40, _minimoRisposteCorrette: 46, _tempoMassimo: 30)
    static private let C =           Patente(_codice: "SUP:CCE",  _nome: "C",          _nDomandeDefault: 40, _minimoRisposteCorrette: 46, _tempoMassimo: 30)
    static private let D =           Patente(_codice: "SUP:DDE",  _nome: "D",          _nDomandeDefault: 40, _minimoRisposteCorrette: 46, _tempoMassimo: 30)
    static private let CQC_Comuni =  Patente(_codice: "CQC:CQCC", _nome: "CQC Comuni", _nDomandeDefault: 60, _minimoRisposteCorrette: 66, _tempoMassimo: 120, _supportaEsamePersonalizzato: false)
    static private let CQC_Merci =   Patente(_codice: "CQC:CQCM", _nome: "CQC Merci",  _nDomandeDefault: 60, _minimoRisposteCorrette: 66, _tempoMassimo: 120)//Not working anymore
    static private let CQC_Persone = Patente(_codice: "CQC:CQCP", _nome: "CQC Persone",_nDomandeDefault: 60, _minimoRisposteCorrette: 66, _tempoMassimo: 120)//Not working anymore
    
    static let Patenti : [(String, Patente)] = [
        ("Patente AM", AM),
        ("Patente A", AB),
        ("Patente B", AB),
        ("Patente C", C),
        ("Patente D", D),
        ("Abilitazione CQC Comune", CQC_Comuni),
        //("Abilitazione CQC Merci", CQC_Merci),
        //("Abilitazione CQC Persone", CQC_Persone),
    ]
    
    enum ResultType
    {
        case Error
        case Success
        case NotComplete
    }
}
