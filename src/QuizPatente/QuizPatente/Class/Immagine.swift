//
//  Immagine.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 24/01/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class Immagine: NSObject
{
    public var ID : Int
    
    var url : URL?
    var image : Data? = nil
    var downQueue : DispatchQueue
    
    init(_id : Int)
    {
        self.ID = _id
        let urlString = String(format: "https://www.quizpatenteapp.com/Cartelli/img%03d.gif", ID)
        self.url = URL(string: urlString)
        self.image = nil
        self.downQueue = DispatchQueue(label: "Image Download Queue")
        super.init()
        
        self.downQueue.async
        {
                self.image = try? Data(contentsOf: self.url!)
        }
    }
    
    func setImage(imageView : UIImageView)
    {
        if self.image != nil
        {
            DispatchQueue.main.async
            {
                imageView.image = #imageLiteral(resourceName: "loading")
            }
        }
        downQueue.async
        {
            DispatchQueue.main.async
            {
                if self.image != nil
                {
                    imageView.image = UIImage(data: self.image!)
                }
                else
                {
                    //TODO imageView.image = immagine non disponibile
                }
            }
        }
    }
}
