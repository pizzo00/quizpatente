//
//  ViewDomanda.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 07/03/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class ViewQuiz: UIViewController //, UIScrollViewDelegate
{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var slides:[ViewDomanda] = [];
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    func setupSlideScrollView(slides : [ViewDomanda])
    {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }

    func createSlides() -> [ViewDomanda]
    {
        var output : [ViewDomanda] = []
        var nDomanda = 0
        for domanda in Static.QuizCorrente!.Domande
        {
            nDomanda += 1
            let slide = Bundle.main.loadNibNamed("ViewDomanda", owner: self, options: nil)?.first as! ViewDomanda
            slide.Domanda = domanda
            slide.nDomanda = nDomanda
            slide.TotDomande = Static.QuizCorrente!.Domande.count
            slide.Parent = self
            slide.redraw()
            output.append(slide)
        }
        
        return output
    }
    
    public func nextPage()
    {
        let slideWidth = self.scrollView.frame.size.width
        UIView.animate(withDuration: 0.3)
        {
            self.scrollView.contentOffset.x += slideWidth
        }
    }
    
    public func goToPage(nPagina: Int)
    {
        let slideWidth = self.scrollView.frame.size.width
        UIView.animate(withDuration: 0.3)
        {
            self.scrollView.contentOffset.x = CGFloat(nPagina) * slideWidth
        }
    }
    
    public func fineQuiz()
    {
        if Static.QuizCorrente!.Domande.contains(where: {$0.RispostaUtente == nil})//Se quiz contiene domande senza risposta
        {
            let domandeMancantiAlert = UIAlertController(title: "Risposte mancanti", message: "Non hai risposto a tutte le domande. Continuare comunque?", preferredStyle: UIAlertController.Style.alert)
            domandeMancantiAlert.addAction(UIAlertAction(title: "No", style: .cancel))
            domandeMancantiAlert.addAction(UIAlertAction(title: "Si", style: .destructive, handler: { (action: UIAlertAction!) in
                self.segueCorrezione()
            }))
            self.present(domandeMancantiAlert, animated: true, completion: nil)
        }
        else
        {
            self.segueCorrezione()
        }
    }
    
    public func segueCorrezione()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "Correzioni") as! UINavigationController
        self.present(viewController, animated: true, completion: nil)
    }
}

