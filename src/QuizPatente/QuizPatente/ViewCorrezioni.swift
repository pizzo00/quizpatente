//
//  ViewCorrezioni.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 07/03/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class ViewCorrezioni: UITableViewController
{
    @IBOutlet weak var lbl_risultato: UILabel!
    @IBOutlet weak var lbl_corrette: UILabel!
    @IBOutlet weak var lbl_nonDate: UILabel!
    @IBOutlet weak var lbl_errate: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let quiz = Static.QuizCorrente!
        let patente = quiz.Patente!
        var corrette = 0
        var errate = 0
        var nonDate = 0
        for domanda in quiz.Domande
        {
            if domanda.RispostaUtente == nil
            {
                nonDate += 1
            }
            else if domanda.RispostaUtente! == domanda.RispostaCorretta
            {
                corrette += 1
            }
            else
            {
                errate += 1
            }
        }
        lbl_nonDate.text = String(nonDate)
        lbl_corrette.text = String(corrette)
        lbl_errate.text = String(errate)
        if patente.NDomandeDefault != quiz.Domande.count
        {
            lbl_risultato.text = "Quiz Personalizzato"
        }
        else if corrette >= patente.MinimoRisposteCorrette
        {
            lbl_risultato.text = "Promosso"
        }
        else
        {
            lbl_risultato.text = "Bocciato"
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 99.5
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return Static.QuizCorrente!.Domande.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "correzioneProtoCell", for: indexPath) as! CellCorrezione
        
        cell.id = indexPath.row
        cell.stampaDomanda(domanda: Static.QuizCorrente!.Domande[cell.id])
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ViewCorrezione"
        {
            let cell = sender as! CellCorrezione
            let domanda = Static.QuizCorrente!.Domande[cell.id]
            let destination = segue.destination as! ViewCorrezione
            destination.downloadCorrezione(domanda: domanda)
        }
    }

    @IBAction func backHome(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "Menu") as! ViewMenu
        self.present(viewController, animated: true, completion: nil)
    }
}
