//
//  ViewDomanda.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 14/03/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class ViewDomanda: UIView
{

    @IBOutlet weak var lbl_hint: UILabel!
    @IBOutlet weak var img_hintArrow: UIImageView!
    @IBOutlet weak var lbl_domanda: UILabel!
    @IBOutlet weak var lbl_numeroDomanda: UILabel!
    @IBOutlet weak var img_immagine: UIImageView!
    @IBOutlet weak var btt_falso: UIButton!
    @IBOutlet weak var btt_vero: UIButton!
    
    public var Domanda: Domanda?
    public var nDomanda: Int = 0
    public var TotDomande: Int = 0
    public var Parent: ViewQuiz?
    
    public func redraw()
    {
        if nDomanda == 1 || nDomanda == TotDomande
        {
            lbl_hint.isHidden = false
            img_hintArrow.isHidden = false
        }
        lbl_domanda.text = Domanda?.Testo ?? ""
        //lbl_domanda.sizeToFit()
        lbl_numeroDomanda.text = String(nDomanda) + "/" + String(TotDomande)
        Domanda?.Figura?.setImage(imageView: img_immagine)
    }
    
    
    @IBAction func veroClick(_ sender: UIButton)
    {
        btt_vero.isHighlighted = false
        btt_falso.isHighlighted = true
        Domanda?.RispostaUtente = true
        nextPage()
    }
    
    @IBAction func falsoClick(_ sender: UIButton)
    {
        btt_vero.isHighlighted = true
        btt_falso.isHighlighted = false
        Domanda?.RispostaUtente = false
        nextPage()
    }
    
    func nextPage()
    {
        if nDomanda < TotDomande
        {
            //Parent?.goToPage(nPagina: nDomanda+1)
            Parent?.nextPage()
        }
    }
    @IBAction func fineQuizClick(_ sender: UIButton)
    {
        Parent?.fineQuiz()
    }
}
