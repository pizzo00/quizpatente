//
//  ViewCorrezioni.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 07/03/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class CellCorrezione: UITableViewCell
{
    @IBOutlet weak var img_immagine: UIImageView!
    @IBOutlet weak var img_statoRisposta: UIImageView!
    @IBOutlet weak var lbl_testo: UILabel!
    @IBOutlet weak var lbl_rispostaUtente: UILabel!
    @IBOutlet weak var lbl_rispostaEsatta: UILabel!
    
    var id: Int = -1
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    public func stampaDomanda(domanda: Domanda)
    {
        //Indicatore validità risposta
        if domanda.RispostaUtente == nil
        {
            img_statoRisposta.image = #imageLiteral(resourceName: "gray")
            lbl_rispostaUtente.textColor = UIColor.black
        }
        else if domanda.RispostaUtente! == domanda.RispostaCorretta
        {
            img_statoRisposta.image = #imageLiteral(resourceName: "green")
            lbl_rispostaUtente.textColor = UIColor.black
        }
        else
        {
            img_statoRisposta.image = #imageLiteral(resourceName: "red")
            lbl_rispostaUtente.textColor = UIColor.red
        }
        
        //Immagine
        if domanda.Figura == nil
        {
            img_immagine.image = nil
        }
        else
        {
            domanda.Figura!.setImage(imageView: img_immagine)
        }
        
        //Risposta data / corretta
        if domanda.RispostaUtente == nil
        {
            lbl_rispostaUtente.text = ""
        }
        else if domanda.RispostaUtente!
        {
            lbl_rispostaUtente.text = "Vero"
        }
        else
        {
            lbl_rispostaUtente.text = "Falso"
        }
        
        if domanda.RispostaCorretta
        {
            lbl_rispostaEsatta.text = "Vero"
        }
        else
        {
            lbl_rispostaEsatta.text = "Falso"
        }
        
        //Testo
        lbl_testo.text = domanda.Testo
    }

}
