//
//  ViewCorrezioni.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 07/03/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class ViewCorrezione: UIViewController {

    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var img_immagine: UIImageView!
    @IBOutlet weak var img_statoRisposta: UIImageView!
    @IBOutlet weak var lbl_domanda: UILabel!
    @IBOutlet weak var lbl_rispostaUtente: UILabel!
    @IBOutlet weak var lbl_rispostaEsatta: UILabel!
    @IBOutlet weak var lbl_titoloCorrezione: UILabel!
    @IBOutlet weak var lbl_testoCorrezione: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loader.startAnimating()
    }
    
    func downloadCorrezione(domanda: Domanda)
    {
        domanda.getCorrezione(
            success: {self.stampa(domanda: domanda)},
            error: {},
            complete: {})
    }
    
    private func stampa(domanda: Domanda)
    {
        DispatchQueue.main.async
        {
            self.loader.stopAnimating()
            self.loader.isHidden = true
            
            domanda.Figura?.setImage(imageView: self.img_immagine)
            self.lbl_domanda.text = domanda.Testo
            
            //Indicatore validità risposta
            if domanda.RispostaUtente == nil
            {
                self.img_statoRisposta.image = #imageLiteral(resourceName: "gray")
            }
            else if domanda.RispostaUtente! == domanda.RispostaCorretta
            {
                self.img_statoRisposta.image = #imageLiteral(resourceName: "green")
            }
            else
            {
                self.img_statoRisposta.image = #imageLiteral(resourceName: "red")
                self.lbl_rispostaUtente.textColor = UIColor.red
            }

            //Risposta data / corretta
            if domanda.RispostaUtente == nil
            {
                self.lbl_rispostaUtente.text = ""
            }
            else if domanda.RispostaUtente!
            {
                self.lbl_rispostaUtente.text = "Vero"
            }
            else
            {
                self.lbl_rispostaUtente.text = "Falso"
            }
            
            if domanda.RispostaCorretta
            {
                self.lbl_rispostaEsatta.text = "Vero"
            }
            else
            {
                self.lbl_rispostaEsatta.text = "Falso"
            }
            
            //Correzione
            self.lbl_titoloCorrezione.text = domanda.Teoria?.Titolo ?? ""
            self.lbl_testoCorrezione.text = domanda.Teoria?.Testo ?? ""
        }
    }
}
