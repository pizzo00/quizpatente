//
//  ViewController.swift
//  QuizPatente
//
//  Created by PIZZOLATO DAVIDE on 24/01/2019.
//  Copyright © 2019 Pizzolato Davide. All rights reserved.
//

import UIKit

class ViewMenu: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource
{
    @IBOutlet weak var picker_patente: UIPickerView!
    @IBOutlet weak var btt_iniziaEsame: UIButton!
    @IBOutlet weak var btt_iniziaEsamePersonalizzato: UIButton!
    @IBOutlet weak var loader_inizaEsame: UIActivityIndicatorView!
    @IBOutlet weak var lbl_numeroDomande: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var lbl_esamePersonalizzatoNonSupportato: UILabel!
    
    
    override func viewDidLoad()
    {
        loader_inizaEsame.isHidden = true //Non funziona la stessa opzione impostata da storyboard
        
        picker_patente.delegate = self
        picker_patente.dataSource = self
        picker_onChange()//Usato per bloccare l'esame personalizzato se la prima patente in lista non lo supporta
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return Static.Patenti.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return Static.Patenti[row].0
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        picker_onChange()
    }
    func picker_onChange()
    {
        let row = picker_patente.selectedRow(inComponent: 0)
        let patente = Static.Patenti[row].1
        bloccaEsamePersonalizzato(sblocca: patente.SupportaEsamePersonalizzato)
    }
    
    @IBAction func inizaEsame()
    {
        let row = picker_patente.selectedRow(inComponent: 0)
        let patente = Static.Patenti[row].1
        Static.QuizCorrente = Quiz(_patente: patente,
                                   _nDomande: patente.NDomandeDefault,
                                   _success: {self.segueDomande()},
                                   _error: {self.endAnimateWithError()},
                                   _complete: {})
        startAnimate()
    }
    
    @IBAction func inizaEsamePersonalizzato()
    {
        let row = picker_patente.selectedRow(inComponent: 0)
        let patente = Static.Patenti[row].1
        Static.QuizCorrente = Quiz(_patente: patente,
                                   _nDomande: Int(stepper.value),
                                   _success: {self.segueDomande()},
                                   _error: {self.endAnimateWithError()},
                                   _complete: {})
        startAnimate()
    }
    
    @IBAction func stepperValueChange()
    {
        lbl_numeroDomande.text = String(Int(stepper.value))
    }
    
    
    func segueDomande()
    {
        DispatchQueue.main.sync
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "Domanda") as! ViewQuiz
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    func bloccaEsamePersonalizzato(sblocca: Bool)
    {
        btt_iniziaEsamePersonalizzato.isEnabled = sblocca
        stepper.isEnabled = sblocca
        lbl_esamePersonalizzatoNonSupportato.isHidden = sblocca
    }
    
    func startAnimate()
    {
        btt_iniziaEsame.isEnabled = false
        btt_iniziaEsamePersonalizzato.isEnabled = false
        stepper.isEnabled = false
        picker_patente.isUserInteractionEnabled = false
        loader_inizaEsame.isHidden = false
        loader_inizaEsame.startAnimating()
    }
    
    func endAnimateWithError()
    {
        DispatchQueue.main.sync
        {
            btt_iniziaEsame.isEnabled = true
            btt_iniziaEsamePersonalizzato.isEnabled = true
            stepper.isEnabled = true
            picker_patente.isUserInteractionEnabled = true
            loader_inizaEsame.isHidden = true
            loader_inizaEsame.stopAnimating()
            
            let erroreGetQuizAlert = UIAlertController(title: "Errore", message: "Errore durante il reperimento del quiz, controllare la connesione internet e riprovare più tardi.", preferredStyle: UIAlertController.Style.alert)
            erroreGetQuizAlert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(erroreGetQuizAlert, animated: true, completion: nil)
        }
    }
    
}

